package Oblig2019.SjekkPassord;


import java.util.Scanner;

public class SjekkPassord
{
    public static void main(String[] args)
    {
        Scanner input = new Scanner(System.in);

        System.out.println("Passordet ditt må inneholde minimum 8 tegn, skal inneholde kun bosktaver og tall, må ha minimum 2 siffer. Skriv inn ditt hemmelige passord:");
        String password = input.next();

        String valid = "";
        if (lengthCheck(password, 8) && characterCheck(password) && formatCheck(password,2))
        {
            valid = "gyldig";
        } else
        {
            valid = "ugyldig";
        }
        System.out.printf("Passordet er %s", valid);
    }

    public static boolean lengthCheck(String password, int minimumLength)
    {
        return password.length() >= minimumLength;
    }

    public static boolean characterCheck(String password)
    {
        for (int i = 0; i < password.length(); i++)
        {
            if (!Character.isLetterOrDigit(password.charAt(i)))
            {
                return false;
            }

        }
        return true;
    }

    public static boolean formatCheck(String password, int minimumNumbers)
    {
        int numbers = 0;
        for (int i = 0; i < password.length(); i++)
        {
            if (Character.isDigit(password.charAt(i)))
            {
                numbers++;

            }

        }
        if (numbers >= minimumNumbers)
        {
            return true;
        } else
        {
            return false;
        }

    }
}




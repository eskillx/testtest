package Oblig2019.ArvForelesning;

public class Circle extends Shape
{
    int radius;
    int x, y;
    boolean fill;

    public Circle(int radius,int x, int y, boolean fill)
    {
        this.radius = radius;
        this.x =x;
        this.y =y;
        this.fill =fill;
    }

    public int getRadius()
    {
        return radius;
    }
}

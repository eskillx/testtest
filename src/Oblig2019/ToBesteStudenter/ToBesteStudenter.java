package Oblig2019.ToBesteStudenter;

import java.util.Scanner;

public class ToBesteStudenter
{
    public static void main(String[] args)
    {
        Scanner input = new Scanner(System.in);

        int score;
        String student = "";
        int bestScore = -65535;
        int nextBestScore = -65535;
        String bestStudent = "";
        String nextBestStudent = "";

        System.out.println("Dette programmet tar in x antall studenter med poengsum, og gir ut hvilke to studenter som gjorde det best");
        System.out.println("Venligst skriv inn antall studenter");

        int numberOfStudents = input.nextInt();

        for (int i = 0; i < numberOfStudents; i++)
        {
            System.out.println("Skriv in poengsummen til studenten, og skriv så inn navnet på studenten");
            score = input.nextInt();
            student = input.nextLine();

            if (score > bestScore)
            {
                nextBestScore = bestScore;
                nextBestStudent = bestStudent;
                bestScore = score;
                bestStudent = student;
            }
            else if (score <= bestScore && score > nextBestScore)
            {
                nextBestScore = score;
                nextBestStudent = student;
            }
        }
        System.out.println("Første plass: " + bestStudent + " med poengsum " + bestScore);
        System.out.println("Andre plass: " + nextBestStudent + " med poengsum " + nextBestScore);
    }
}

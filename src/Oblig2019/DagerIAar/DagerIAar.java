package Oblig2019.DagerIAar;

public class DagerIAar
{
    public static void main(String[] args)
    {


        System.out.println("Dette programmet forteller hvor mange dager det er i et år mellom 2000 og 2020");
        for (int i = 2000; i<=2020; i++)
        {
            System.out.println("Year " + i + " has " + daysInAYear(i)+ " days.");
        }
    }

    public static int daysInAYear(int year)
    {
        int days = 0;

        if ((year % 4 == 0 && year % 100 != 0) || (year % 400 == 0))// is a leap year if year is dividable by 4 and not 100 or if year is dividable by 400
        {
            days = 366;
        }
        else days = 365;
        return days;
    }

}

package Oblig2019.Flyreservasjon;

import java.util.Scanner;

public class FlyReservasjon
{

    public static void main(String[] args)
    {

        Scanner input = new Scanner(System.in);
        FlyReservasjon Flyreservasjon1 = new FlyReservasjon();

        while (true)
        {
            Flyreservasjon1.show();
            int valgtRad = -1;

            while (!Flyreservasjon1.radOk(valgtRad))
            {
                System.out.println("Hvilken rad vil du sitte på?");
                valgtRad = input.nextInt();
                if (valgtRad==-1){System.exit(-1);}

                if (!Flyreservasjon1.radOk(valgtRad))
                {
                    System.out.println("Valgt rad er ugyldig");
                }
            }
            char valgtSete = 'T';

            while (!Flyreservasjon1.getRad(valgtRad).isLegalSeat(valgtSete))
            {
                System.out.println("Hvilket sete vil du sitte på?");
                valgtSete = input.next().toUpperCase().charAt(0);
                if (valgtSete=='-'){System.exit(-1); }
            if (!Flyreservasjon1.getRad(valgtRad).isLegalSeat(valgtSete))
                {
                    System.out.println("Valgt sete er ugyldig");
                }
            }
            Flyreservasjon1.reserveSeat(valgtRad, valgtSete);


        }
    }


    private Rad[] fly;

    public FlyReservasjon()
    {
        this.fly = new Rad[10];

        for (int i = 0; i < 10; i++)
        {
            fly[i] = new Rad();
        }

    }

    public FlyReservasjon(int antallRader)
    {
        this.fly = new Rad[antallRader];
        for (int i = 0; i < antallRader; i++)
        {
            fly[i] = new Rad();
        }
    }

    public FlyReservasjon(int antallRader, int antallSeterPerRad)

    {


        this.fly = new Rad[antallRader];

        for (int i = 0; i <fly.length; i++)
        {
            fly[i] = new Rad(antallSeterPerRad);

        }

    }

    public boolean radOk(int valgtRad)
    {
        return (valgtRad < fly.length && valgtRad >= 0);
    }

    public Rad getRad(int valgtRad)
    {
        return fly[valgtRad-1];
    }

    public void show()
    {
        for (int i = 0; i < fly.length; i++)
        {
            System.out.print((i + 1) + "  ");

            fly[i].printSeats();

            System.out.println();

        }

    }

    public boolean reserveSeat(int rad, char seat)
    {
        if (radOk(rad))
            return fly[rad - 1].assignSeat(seat);

        else
        {
            return false;

        }
    }


}

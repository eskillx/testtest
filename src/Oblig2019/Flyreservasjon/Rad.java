package Oblig2019.Flyreservasjon;

public class Rad
{
    private Character[] rad;
    private int antallSeter;

    public Rad()
    {
        this.rad = new Character[4];
        this.antallSeter = this.rad.length;

        for (int i = 0; i < rad.length; i++)
        {
            this.rad[i] = (char) (65 + i);
        }
    }

    public Rad(int seats)
    {
        if (seats > 10)
        {
            System.out.println("maksimalt antall seter er 10, antall seter er satt til 10");
            seats = 10;
        }
        this.rad = new Character[seats];
        this.antallSeter = this.rad.length;

        for (int i = 0; i < rad.length; i++)
        {
            this.rad[i] = (char) (65 + i);
        }
    }
    public int getNumberOfSeats()
    {
        return antallSeter;
    }
    public char getSeatAtIndex (int index)
    {
        return this.rad[index];
    }

    public boolean isLegalSeat(char seat)
    {
       return(seat>='A'&&seat<=(char)(65+this.getNumberOfSeats()-1)&&(rad[seat-65]!='X'));
    }

    public boolean assignSeat(char seat)
    {
        if (isLegalSeat(seat)&&(rad[seat-65]!='X'))
        {
            rad[seat-65] = 'X';
            return true;
        }

        return false;
    }

    public void printSeats()
    {
        for (int i = 0; i < getNumberOfSeats(); i++)
        {
            System.out.print(Character.toString(getSeatAtIndex(i)));
            System.out.print("  ");
        }

    }
}

package Oblig2019.SummerDiagonalIMatrise;

import java.util.Scanner;

public class SummerDiagonalIMatrise
{

    public static void main(String[] args)
    {
        Scanner input = new Scanner(System.in);

        int[][] matrise = new int[4][4];
        System.out.println("Vennligst oppgi en 4x4 matrise");
        for (int i = 0; i <4; i++)
        {
            for (int j = 0; j <4; j++)
            {
                matrise[i][j] = input.nextInt();
            }

        }

        double sumDiagonal = 0;
        for (int i = 0; i <4; i++)
        {
            sumDiagonal =+ matrise[i][i];

        }
        double snittDiagonal = sumDiagonal/4.0;
        System.out.printf("Summen av diagonalen er: %.2f, og gjennomsnittet av diagonalen er %.2f",sumDiagonal,snittDiagonal);
    }
}

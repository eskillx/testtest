package Oblig2019.KredittkortValdering;

import java.util.Scanner;

public class KredittkortValideringV2
{
    public static void main(String[] args)
    {
        Scanner input = new Scanner(System.in);

        System.out.print("Enter a credit card number as a long integer: ");
        long number = input.nextLong();

        int[] extractedNumbers;
        extractedNumbers = new int[getSize(number)];
        for (int i = 0; i < extractedNumbers.length; i++)
        {
            extractedNumbers[i] = extractNumbers(number, i);
        }

        if (isValid(number))
        {
            System.out.println(number + " is valid");
        }
        else
        {
            System.out.println(number + " is invalid");
        }
    }

    /**
     * Return true if the card number is valid
     */
    public static boolean isValid(long number)
    {
        return (getSize(number) >= 13) && (getSize(number) <= 16) &&
                (prefixMatched(number, 4) || prefixMatched(number, 5) ||
                        prefixMatched(number, 6) || prefixMatched(number, 37)) &&
                (sumOfDoubleEvenPlace(number) + sumOfOddPlace(number)) % 10 == 0;
    }

    public static int extractNumbers(long number, int spot)
    {
        String numberS = String.valueOf(number);
        int extractedNumber = Integer.parseInt(String.valueOf(numberS.charAt(spot)));
        return extractedNumber;

    }

    public static int getSize(long number)
    {
        String SizeCheck = String.valueOf(number);
        return SizeCheck.length();

    }

    public static boolean prefixMatched(long number, int checksum)
    {
        String prefixCheck = String.valueOf(number);
        boolean valid = false;
        if (checksum < 10)
        {
            int check = Integer.parseInt(String.valueOf(prefixCheck.charAt(0)));
            valid = check == checksum;
        }
        if (checksum >= 10)
        {
            int check = (Integer.parseInt(String.valueOf(prefixCheck.charAt(0))) * 10) + Integer.parseInt(String.valueOf(prefixCheck.charAt(1)));
            valid = check == checksum;

        }
        System.out.println(valid);
        return valid;
    }

    public static int sumOfDoubleEvenPlace(long number)
    {
        String numberS = String.valueOf(number);
        int zeroP = 0;
        int twoP = 0;
        int fourP = 0;
        int sixP = 0;
        int eightP = 0;
        int tenP = 0;
        int twelveP = 0;
        int fourteenP = 0;

        if (numberS.length() > 1)
        {
            zeroP = Integer.parseInt(String.valueOf(numberS.charAt(numberS.length() - 2))) * 2;
            if (zeroP >= 10)
            {
                zeroP = zeroP % 10 + 1;
            }
        }
        if (numberS.length() > 3)
        {
            twoP = Integer.parseInt(String.valueOf(numberS.charAt(numberS.length() - 4))) * 2;
            if (twoP >= 10)
            {
                twoP = twoP % 10 + 1;
            }
        }
        if (numberS.length() > 5)
        {
            fourP = Integer.parseInt(String.valueOf(numberS.charAt(numberS.length() - 6))) * 2;
            if (fourP >= 10)
            {
                fourP = fourP % 10 + 1;
            }
        }
        if (numberS.length() > 7)
        {
            sixP = Integer.parseInt(String.valueOf(numberS.charAt(numberS.length() - 8))) * 2;
            if (sixP >= 10)
            {
                sixP = sixP % 10 + 1;
            }
        }
        if (numberS.length() > 9)
        {

            eightP = Integer.parseInt(String.valueOf(numberS.charAt(numberS.length() - 10))) * 2;
            if (eightP >= 10)
            {
                eightP = eightP % 10 + 1;
            }
        }
        if (numberS.length() > 11)
        {
            tenP = Integer.parseInt(String.valueOf(numberS.charAt(numberS.length() - 12))) * 2;
            if (tenP >= 10)
            {
                tenP = tenP % 10 + 1;
            }
        }
        if (numberS.length() > 13)
        {
            twelveP = Integer.parseInt(String.valueOf(numberS.charAt(numberS.length() - 14))) * 2;
            if (twelveP >= 10)
            {
                twelveP = twelveP % 10 + 1;
            }
        }
        if (numberS.length() > 15)
        {
            fourteenP = Integer.parseInt(String.valueOf(numberS.charAt(numberS.length() - 16))) * 2;
            if (fourteenP >= 10)
            {
                fourteenP = fourteenP % 10 + 1;
            }
        }
        int sumDoubleEvenPlace = (zeroP + twoP + fourP + sixP + eightP + tenP + twelveP + fourteenP);
        System.out.println(sumDoubleEvenPlace);
        return sumDoubleEvenPlace;
    }


    public static int sumOfOddPlace(long number)
    {
        String numberS = String.valueOf(number);
        int oneP = 0;
        int threeP = 0;
        int fiveP = 0;
        int sevenP = 0;
        int nineP = 0;
        int elevenP = 0;
        int thirteenP = 0;
        int fifteenP = 0;

        if (numberS.length() > 1)
        {
            oneP = Integer.parseInt(String.valueOf(numberS.charAt(numberS.length() - 1)));
        }
        if (numberS.length() > 3)
        {
            threeP = Integer.parseInt(String.valueOf(numberS.charAt(numberS.length() - 3)));
        }
        if (numberS.length() > 5)
        {
            fiveP = Integer.parseInt(String.valueOf(numberS.charAt(numberS.length() - 5)));
        }
        if (numberS.length() > 7)
        {
            sevenP = Integer.parseInt(String.valueOf(numberS.charAt(numberS.length() - 7)));
        }
        if (numberS.length() > 9)
        {
            nineP = Integer.parseInt(String.valueOf(numberS.charAt(numberS.length() - 9)));
        }
        if (numberS.length() > 11)
        {
            elevenP = Integer.parseInt(String.valueOf(numberS.charAt(numberS.length() - 11)));
        }
        if (numberS.length() > 13)
        {
            thirteenP = Integer.parseInt(String.valueOf(numberS.charAt(numberS.length() - 13)));
        }
        if (numberS.length() > 15)
        {
            fifteenP = Integer.parseInt(String.valueOf(numberS.charAt(numberS.length() - 15)));
        }
        int sumOfEvenPlace = (oneP + threeP + fiveP + sevenP + nineP + elevenP + thirteenP + fifteenP);
        System.out.println(sumOfEvenPlace);
        return sumOfEvenPlace;
    }
}



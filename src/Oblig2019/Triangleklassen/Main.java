package Oblig2019.Triangleklassen;

public class Main
{
    public static void main(String[] args)
    {

            Triangle triangle1 = new Triangle(3, 4, 5);
            Triangle triangle2 = new Triangle(3, 4, 12);
            Triangle triangle3 = new Triangle(3, 6, 12);


        if (triangle1.checkLegality(3, 4, 5))
        {
            System.out.println("Trekant1 sitt areal er: " + triangle1.getArea());
            System.out.println("Trekant1 sin omkrets er: " + triangle1.getPerimiter());
            System.out.println(triangle1.toString());
        }
        else
        {
            System.out.println("Dette er en ugyldig trekant");
        }
        if (triangle2.checkLegality(3, 4, 12))
        {
            System.out.println("Trekant2 sitt areal er: " + triangle2.getArea());
            System.out.println("Trekant2 sin omkrets er: " + triangle2.getPerimiter());
            System.out.println(triangle2.toString());
        }
        else
        {
            System.out.println("Dette er en ugyldig trekant");
        }
        if (triangle3.checkLegality(3, 6, 12))
        {
            System.out.println("Trekant3 sitt areal er: " + triangle3.getArea());
            System.out.println("Trekant3 sin omkrets er: " + triangle3.getPerimiter());
            System.out.println(triangle3.toString());
        }
        else
        {
            System.out.println("Dette er en ugyldig trekant");
        }
    }
}

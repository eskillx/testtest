package Oblig2019.ReverserArray;

import java.util.Scanner;

public class RevererArray
{
    public static void main(String[] args)
    {
        Scanner input = new Scanner(System.in);
        int[] array = new int[10];
        System.out.println("skriv inn 10 verdier");
        for (int i = 0; i < 10; i++)
        {
            array[i] = input.nextInt();
        }
        for (int i = 0; i < array.length; i++)
        {
            System.out.println(array[i]);

        }
        for (int i = 0; i < array.length - 1; i++)
        {
            array[i] = reverse(array)[i];
        }
        for (int i = 0; i < array.length; i++)
        {
            System.out.println(array[i]);
        }
    }

    public static int[] reverse(int[] list)
    {
        for (int i = 0; i < (list.length) / 2; i++)
        {
            int plass1 = list[i];
            int plass2 = list[(list.length - 1) - i];
            list[i] = plass2;
            list[(list.length - 1) - i] = plass1;

        }
        return list;
    }
}

package Oblig2019.MyRectangle;

public class MyRectangle
{
    private double width;
    private double height;

    public MyRectangle()
    {
        this.width = 1;
        this.height = 1;
    }

    public MyRectangle(double height, double width)
    {
        this.height = height;
        this.width = width;
    }

    public double getWidth()
    {
        return width;
    }

    public void setHeight(double height)
    {
        this.height = height;
    }

    public void setWidth(double width)
    {
        this.width = width;
    }

    public double getHeight()
    {
        return height;
    }

    public double getArea()
    {
        return width * height;
    }

    public double getPerimeter()
    {
        return 2 * width + 2 * height;
    }


    public static void main(String[] args)
    {
        MyRectangle rectangle1 = new MyRectangle(4,40);

        System.out.println(rectangle1.getArea());
        System.out.println(rectangle1.getPerimeter());
        MyRectangle rectangle2 = new MyRectangle();

        rectangle2.setHeight(6.30);
        rectangle2.setWidth(5.40);
        System.out.println(rectangle2.getArea());
        System.out.println(rectangle2.getPerimeter());

    }

}
package Oblig2019.SteinSaksPapir;


import java.util.Scanner;


public class SteinSaksPapir
{
    public static void main(String[] args)
    {
        Scanner input = new Scanner(System.in);

        System.out.println("La oss spille stein, saks, papir. Velg mellom Stein[0], Saks[1] eller Papir[2]");
        int userChoise = input.nextInt();

        if (userChoise < 0 || userChoise > 2)
        {
            System.out.println("Dette er ikke et gyldig valg");
            main(null);
        }

        int computerChoise = (int) (Math.random() * 3);

        switch (userChoise)
        {
            case 0: // spiller har valgt stein
                if (computerChoise == 0)
                {
                    System.out.println("Maskinen valgte også stein, det ble uavgjort!");
                } else if (computerChoise == 1)
                {
                    System.out.println("Maskinen valgte saks, du vant!");
                } else if (computerChoise == 2)
                {
                    System.out.println("Maksinen valgte papir, du tapte!");
                }
                break;
            case 1: // spiller har valgt saks
                if (computerChoise == 0)
                {
                    System.out.println("Maskinen valgte stein, du tapte!");
                } else if (computerChoise == 1)
                {
                    System.out.println("Maskinen valgte også saks, det ble uavgjort!");
                } else if (computerChoise == 2)
                {
                    System.out.println("maksinen valgte papir, du vant!");

                }
                break;
            case 2: //spiller har valgt papir
                if (computerChoise == 0)
                {
                    System.out.println("Maskinen valgte stein, du vant!");
                } else if (computerChoise == 1)
                {
                    System.out.println("Maskinen valgte saks, du tapte!");
                } else if (computerChoise == 2)
                {
                    System.out.println("Maksinen valgte også papir, det ble uavgjort!");
                }
                break;
        }
        System.out.println("Vil du spille på nytt? trykk [1], hvis ikke trykk hvilket som helst tall");
        int restart = input.nextInt();

        if (restart == 1)
        {
            main(null);
        } else
        {
            System.out.println("Takk for at du spilte!");
        }
    }
}
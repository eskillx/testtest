package Oblig2019.SteinSaksPapir;


import java.util.Scanner;


public class SteinSaksPapirV2
{
    public static void main(String[] args)
    {
        Scanner input = new Scanner(System.in);
        boolean running = true;

        while (running = true)
        {

            System.out.println("La oss spille stein, saks, papir. Velg mellom Stein[0], Saks[1] eller Papir[2]");

            int userChoise = input.nextInt();
            while (userChoise < 0 || userChoise > 2)
            {

                System.out.println("Dette er ikke et gyldig valg, prøv igjen");
                System.out.println("Velg mellom Stein[0], Saks[1] eller Papir[2]");
                userChoise = input.nextInt();
            }

            int computerChoise = (int) (Math.random() * 3);

            if (computerChoise == userChoise)
            {
                System.out.println("Det ble uavgjort!");
            } else
            {
                switch (userChoise)
                {
                    case 0: // spiller har valgt stein
                        if (computerChoise == 1)
                        {
                            System.out.println("Maskinen valgte saks, du vant!");
                        } else if (computerChoise == 2)
                        {
                            System.out.println("Maksinen valgte papir, du tapte!");
                        }
                        break;
                    case 1: // spiller har valgt saks
                        if (computerChoise == 0)
                        {
                            System.out.println("Maskinen valgte stein, du tapte!");
                        } else if (computerChoise == 2)
                        {
                            System.out.println("maksinen valgte papir, du vant!");

                        }
                        break;
                    case 2: //spiller har valgt papir
                        if (computerChoise == 0)
                        {
                            System.out.println("Maskinen valgte stein, du vant!");
                        } else if (computerChoise == 1)
                        {
                            System.out.println("Maskinen valgte saks, du tapte!");
                        }
                        break;
                }
            }
            System.out.println("Vil du spille på nytt? trykk [1], hvis ikke trykk hvilket som helst tall");
            int restart = input.nextInt();

            if (restart != 1)
            {
                System.out.println("Takk for at du spilte!");
                running = false;
            } else
            {
                System.out.println("Da kjører vi igjen!");
            }
        }
    }
}
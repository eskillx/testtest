package Oblig2019.MergeSortedArrays;

import java.util.ArrayList;
import java.util.Collections;


public class MergeSortedArrays
{

    public static void main(String[] args)
    {
        ArrayList<Integer> list1 = new ArrayList<Integer>();
        ArrayList<Integer> list2 = new ArrayList<Integer>();

        for (int i = 0; i < 10; i++)
        {
            int element = (int) Math.round(Math.random() * 10);
            list1.add(i, element);
        }
        for (int i = 0; i < 10; i++)
        {
            int element = (int) Math.round(Math.random() * 10);
            list2.add(i, element);
        }

        for (int i = 0; i < list1.size(); i++)
        {
            System.out.print(list1.get(i));

        }
        System.out.println();
        for (int i = 0; i < list2.size(); i++)
        {
            System.out.print(list2.get(i));

        }

        Sort(list1);
        Sort(list2);

        System.out.println();
        for (int i = 0; i < list1.size(); i++)
        {
            System.out.print(list1.get(i));

        }
        System.out.println();
        for (int i = 0; i < list2.size(); i++)
        {
            System.out.print(list2.get(i));

        }
        System.out.println();
        int[] SortedArray = mergeToSortedArray(list1 ,list2);
        for (int value : SortedArray)
        {
            System.out.print(value);
        }

    }

    public static ArrayList Sort(ArrayList list)
    {
        Collections.sort(list);
        return list;
    }
    public static int[] mergeToSortedArray(ArrayList list1, ArrayList list2)
    {
        ArrayList temp = new ArrayList();
        for (int i = 0; i <list1.size() ; i++)
        {
            temp.add(list1.get(i));
        }
        for (int i = 0; i <list2.size(); i++)
        {
            temp.add(list2.get(i));
        }
        Sort(temp);
        int[] SortedArray = new int[temp.size()];
        for (int i = 0; i <SortedArray.length; i++)
        {
            SortedArray[i] = (int)temp.get(i);
        }
        return SortedArray;
    }
}

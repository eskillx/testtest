package Oblig2019.OrdneTreOrd;

import java.util.Scanner;

public class OrdneTreOrd
{
    public static void main(String[] args)
    {
        Scanner input = new Scanner(System.in);
        System.out.println("Dette programmet sorterer tre ord i alfabetisk rekkefølge(funker ikke med Æ Ø Å, venligst skriv inn tre ord:");
        String wordOne = input.next();
        String wordTwo = input.next();
        String wordThree = input.next();

        int firstCharAtOne = wordOne.toLowerCase().charAt(0);
        int firstCharAtTwo = wordTwo.toLowerCase().charAt(0);
        int firstCharAtThree = wordThree.toLowerCase().charAt(0);

        System.out.println(firstCharAtOne + " " + firstCharAtTwo + " " + firstCharAtThree);
        if (firstCharAtOne < firstCharAtTwo && firstCharAtOne < firstCharAtThree)
        {
            System.out.println(wordOne);
            if (firstCharAtTwo < firstCharAtThree)
            {
                System.out.println(wordTwo);
                System.out.println(wordThree);
            }
            else
            {
                System.out.println(wordThree);
                System.out.println(wordTwo);
            }
        }
        else if (firstCharAtTwo < firstCharAtOne && firstCharAtTwo < firstCharAtThree)
        {
            System.out.println(wordTwo);
            if (firstCharAtOne < firstCharAtThree)
            {
                System.out.println(wordOne);
                System.out.println(wordThree);
            }
            else
            {
                System.out.println(wordThree);
                System.out.println(wordOne);
            }
        }
        else
        {
            System.out.println(wordThree);
            if (firstCharAtOne < firstCharAtTwo)
            {
                System.out.println(wordOne);
                System.out.println(wordTwo);
            }
            else
            {
                System.out.println(wordTwo);
                System.out.println(wordOne);
            }
        }
    }
}

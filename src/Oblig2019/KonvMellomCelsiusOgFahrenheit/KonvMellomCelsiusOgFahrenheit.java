package Oblig2019.KonvMellomCelsiusOgFahrenheit;

public class KonvMellomCelsiusOgFahrenheit
{
    public static void main(String[] args)
    {
        System.out.println("Celsius\t\tFahrenheit\tFahrenheit\tCeisus");
        for (double i = 40, j=120; i >= 31 && j>=30; i--, j-=10)
        {
            System.out.printf("%.1f\t\t%.1f\t\t",i,CelsiusToFahrenheit(i));
            System.out.printf("%.1f\t\t%.1f\n",j,FahreneheitToCelsius(j));
        }
    }

        public static double CelsiusToFahrenheit ( double celsius)
        {
            return (9.0 / 5) * celsius + 32;
        }
        public static double FahreneheitToCelsius ( double fahrenheit)
        {
            return (5.0 / 9) * (fahrenheit - 32);
        }

}
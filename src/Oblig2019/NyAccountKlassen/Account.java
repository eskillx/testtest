package Oblig2019.NyAccountKlassen;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class Account
{
    private ArrayList<Transaction> transactions;// legg inn en referanse til en ArrayList av Transaction objekter
    private static double annualInterestRate;
    private double balance;
    private Calendar dateCreated;
    private int id;
    private String name;


    public Account(String name, int id, double balance)
    {
        this.name = name;
        this.id = id;
        this.balance = balance;
        this.dateCreated = new GregorianCalendar();
        this.transactions = new ArrayList<>();// opprett arraylist av Transaction objekter
    }

    public ArrayList<Transaction> getTransactions()
    {
        return transactions;
    }

    public Calendar getDateCreated()
    {
        return dateCreated;
    }

    public double getBalance()
    {
        return balance;
    }

    public String getName()
    {
        return name;
    }

    public int getID()
    {
        return id;
    }

    public void setBalance(double balance)
    {
        this.balance = balance;
    }

    public void setID(int id)
    {
        this.id = id;
    }

    public static double getAnnualInterestRate()
    {
        return annualInterestRate;
    }

    public double getMonthlyInterest()
    {
        return balance * (annualInterestRate / 1200);
    }

    public static void setAnnualInterestRate(double newAnnualInterestRate)
    {
        annualInterestRate = newAnnualInterestRate;
    }

    public void withdraw(double amount, String description)
    {
        balance -= amount;
        transactions.add(new Transaction('w', amount, balance, description));
    }

    public void deposit(double amount, String description)
    {
        balance += amount;
        transactions.add(new Transaction('d', amount, balance, description));
    }

    @Override
    public String toString()
    {
        return String.format("Name: %s\n ID: %d\n Balance: %.2f\n Date created: %tF %4$tT", name, id, balance, dateCreated);
    }

    public static void main(String[] args)
    {
        Account ac1 = new Account("Eskil", 2011, 5000);

        ac1.deposit(2000, "Julegave fra fam");
        ac1.withdraw(2000, "Kjøpe mat");
        ac1.deposit(15000, "lønn");
        ac1.withdraw(10000, "ferietur");

        System.out.println(ac1.toString());
        System.out.printf("%s %24s %12s %13s %15s\n", "Date", "Type", "Amount", "Balance", "Description");
        for (Transaction i : ac1.getTransactions())
        {
            System.out.println(i);
        }

    }

}
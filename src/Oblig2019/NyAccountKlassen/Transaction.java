package Oblig2019.NyAccountKlassen;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class Transaction
{
    Calendar transactionDate;
    char direction;
    double amount;
    double balance;
    String description;



    public  Transaction (char direction, double amount, double newBalance, String description)
    {
        this.transactionDate = new GregorianCalendar();
        this.direction = direction;
        this.amount = amount;
        this.balance = newBalance;
        this.description = description;

    }
    public Calendar getTransactionDate()
    {
        return transactionDate;
    }

    public char getDirection()
    {
        return direction;
    }

    public double getAmount()
    {
        return amount;
    }

    public double getBalance()
    {
        return balance;
    }

    public String getDescription()
    {
        return description;
    }
    @Override
    public String toString()
    {
        return String.format("%tF %tT %7c %15.1f %12.1f      %-1s",transactionDate, transactionDate , direction, amount, balance, description );
    }
/* Transaction klassen skal ha følgende felter og metoder:
    java.util.Date date for å indikere når transaksjon ble utført.
    char type 'W' for uttak og 'D' for innskudd, det skal sendes til konstruktør og betegne om transaksjonen er et innskudd eller uttak.
    double amount, hvor mye penger er involvert i transaksjonen.
    double balance, saldo på konto etter transaksjonen.
    String description, beskrivelse av transaksjonen, for eksempel "Bus ticket" eller "Shopping".
    public Get metoder for alle felter
    Overskriv toString metoden for å kunne enkelt skrive ut transaksjonen til skjerm. Se på kjøreeksempel for å få en ide på hvordan det kan gjøres.
    Selve Account klassen skal ha en ArrayList<Transaction> som logger alle transaksjoner.
    Du skal legge til et String felt kalt name for å holde navn til personen som eier kontoen.
    Modifiser withdraw og deposit metoder for å legge til transaksjoner til listen. Disse metodene må altså ta inn de data som du trenger for å lage et Transaction objekt, som du så legger inn i arraylisten som beskrevet over.
    Test klassen skal opprette et Account objekt med navn, start saldo, id, deretter skal du sette annualInterestRate, utføre tre uttak og tre innskudd og skrive ut alle transaksjoner som du henter */
}

package Oblig2019.TilnaermingTilPI;

public class TilnaermingTilPI
{
    public static void main(String[] args)
    {
        double PI = 0;
        double sum = 0;

        for (int i = 1; i <= 100000; i++)
        {
                sum += ((Math.pow(-1, i + 1) / (2 * i - 1)));

            PI = 4 * sum;
            if (i % 10000 == 0)
            {
                System.out.println("For ledd " + i + " i tilnærming til pi, er pi: " + PI);
        }
        }
    }
}
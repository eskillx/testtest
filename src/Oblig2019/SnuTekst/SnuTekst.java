package Oblig2019.SnuTekst;

import java.util.Scanner;

public class SnuTekst
{
    public static void main(String[] args)
    {
        Scanner input = new Scanner(System.in);

        String text = input.nextLine();
        String reverseText = "";

        int stringLength = text.length();

        for (int i = stringLength-1 ; i >= 0; i--)
        {
            reverseText += text.charAt(i);
        }
        System.out.println(reverseText);
    }
}

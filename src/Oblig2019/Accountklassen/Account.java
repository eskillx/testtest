package Oblig2019.Accountklassen;

import java.util.Date;
import java.util.GregorianCalendar;


public class Account
{
    private int id;
    private double balance;
    private GregorianCalendar dateCreated;
    private static double interest;
    public Account(int id, double balance)
    {
        this.id = id;
        this.balance = balance;
        this.dateCreated = new GregorianCalendar();

    }

    public double getBalance()
    {
        return balance;
    }

    public int getId()
    {
        return id;
    }

    public Date getDateCreated()
    {
        return dateCreated.getTime();
    }

    public boolean withdraw(double amount)
    {
        if (amount > balance)
        {
            return false;
        }
        else
        {
            balance = -amount;
            return true;
        }
    }

    public static void setAnnualInterestRate(double interest)
    {
        Account.interest =interest;
    }

    public double getMonthlyInterest()
    {
        return balance * (interest / 1200);
    }

    public boolean deposit(double amount)
    {
        if (amount > 0)
        {
            balance = +amount;
            return true;
        }
        else
        {
            return false;
        }
    }

    public static void main(String[] args)
    {
        Account account1 = new Account(1121, 2000);

        if (account1.withdraw(2500))
        {
            System.out.println("Prøver å ta ut 2500kr, status: Suksess");

        }
        else
        {
            System.out.println("Prøver å ta ut 2500kr, status: Feilet");
        }
        if (account1.deposit(3000))
        {
            System.out.println("Prøver å sette inn 3000kr, Status: Suksess");
        }
        else
        {
            System.out.println("Prøver å sette inn 3000kr, Status: Feilet");
        }
        System.out.printf("Saldoen er: %f.00\n", account1.getBalance());
        Account.setAnnualInterestRate(4.5);
        System.out.printf("Månedlig rente er %f.00 kr\n",account1.getMonthlyInterest());
        System.out.printf("Denne kontoen ble opprettet %s",account1.getDateCreated());
    }
}


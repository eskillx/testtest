package Oblig2019.BeanMachine;

import java.util.Scanner;

public class BeanMachine
{

    public static void main(String[] args)
    {
        Scanner input = new Scanner(System.in);

        System.out.println("Oppgi antall spor");
        int antSpor = input.nextInt();
        System.out.println("Oppgi antall baller:");
        int antBaller = input.nextInt();


        int[] spor = new int[antSpor];

        for (int i = 0; i < antBaller; i++)
        {
            spor[getPathForOneBall(antSpor)]++;
        }
        for (int i = 0; i < spor.length; i++)
        {
            System.out.println(spor[i]);

        }
        max(spor);
        printResult(spor);
    }

    // Genererer en tilfeldig sti for et ball, returnerer indeksen for søylen som ballet har havnet inni.
    private static int getPathForOneBall(int antSpor)
    {
        int ballISpor = 0;
        String path = "";
        for (int i = 0; i < antSpor - 1; i++)
        {
            boolean left = (Math.random() < 0.5);
            if (left)
            {
                path += 'L';
            }
            else
            {
                ballISpor++;
                path += 'R';
            }


        }
        System.out.println(path);
        return ballISpor;
    }

    // Skriver ut søyler med sine baller til skjerm.
    private static void printResult(int[] spor)
    {
        int indexMax = max(spor);
        int max = spor[indexMax];
        int tempmax = spor[indexMax];
        for (int rad = 0; rad <= max - 1; rad++)//antall rader
        {
            for (int kol = 0; kol < spor.length; kol++)//kollonne
            {
                if (spor[kol] >= tempmax)
                {
                    System.out.print("0");
                    spor[kol]--;
                }
                else
                {
                    System.out.print("_");
                }
                if (kol > 9 )
                {
                    System.out.print("  ");
                }

                else
                {
                    System.out.print(" ");
                }
            }
            tempmax--;
            System.out.println();


        }
        int number = 0;
        for (int i = 0; i < spor.length; i++)
        {
            System.out.print(number);
            System.out.print(" ");
            number++;
        }
    }

    // Returnerer indeks for søylen med flest baller. Brukes i printResult metoden.
    private static int max(int[] slots)
    {
        int maks = 0;
        int maksVerdi = 0;
        for (int i = 0; i < slots.length - 1; i++)
        {
            if (slots[i] > maksVerdi)
            {
                maks = i;
                maksVerdi = slots[i];
            }
        }
        System.out.println("høyeste verdi ligger på indeks " + maks);
        System.out.println("høyeste verdi er " + maksVerdi);
        return maks;

    }
}


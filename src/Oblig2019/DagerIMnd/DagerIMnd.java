package Oblig2019.DagerIMnd;

import java.util.Scanner;

public class DagerIMnd
{
    public static void main(String[] args)
    {
        Scanner input = new Scanner(System.in);

        System.out.println("This program tells you how many days there are/were in a month between year 0 and 9999");
        System.out.println("What year do you want to check?");
        int year = input.nextInt();

        while (0 > year || year > 9999)
        {
            System.out.println("This is not a year between year 0 and 9999");
            year = input.nextInt();
        }

        System.out.println("Now give me the month you want to check.");

        String month = input.next().toLowerCase();
        System.out.println(month);
        while (!month.matches("jan|feb|mar|apr|may|jun|jul|aug|sep|oct|nov|dec"))
        {
            System.out.println("You have not written in a month in the right format, use the three first letters in the english name of the month");
            month = input.next().toLowerCase();
        }
        switch (month)
        {
            case ("jan"):
                System.out.println("January " + year + " has 31 days");
                break;
            case ("feb"):
                if ((year % 4 == 0 && year % 100 != 0) || (year % 400 == 0)) // is a leap year if year is dividable by 4 and not 100 or if year is dividable by 400
                {
                    System.out.println("Febrauary " + year + " has 29 days");
                } else
                {
                    System.out.println("Febrauary " + year + " has 28 days");
                }
                break;
            case ("mar"):
                System.out.println("March " + year + " has 31 days");
                break;
            case ("apr"):
                System.out.println("April " + year + " has 30 days");
                break;
            case ("may"):
                System.out.println("May " + year + " has 31 days");
                break;
            case ("jun"):
                System.out.println("June " + year + " has 30 days");
                break;
            case ("jul"):
                System.out.println("July " + year + " has 31 days");
                break;
            case ("aug"):
                System.out.println("August " + year + " has 31 days");
                break;
            case ("sep"):
                System.out.println("September " + year + " has 30 days");
                break;
            case ("oct"):
                System.out.println("October " + year + " has 31 days");
                break;
            case ("nov"):
                System.out.println("November " + year + " has 30 days");
                break;
            case ("dec"):
                System.out.println("December " + year + " has 31 days");
                break;

        }

    }


}


package Oblig2019.EnhetstestForMetodePassord;

import java.util.Scanner;

public class PassordSjekkMetoder
{

    public static void main(String[] args)
    {
        Scanner input = new Scanner(System.in);
        System.out.println("Skriv inn passordet ditt for å sjekke om det er et gyldig passord");
        String passord = input.next();

        String tekst = "ugyldig";
        if (sjekkPassord(passord)){
            tekst = "gyldig";
        }

        System.out.printf("passordet ditt er %s",tekst);
    }

    public static boolean sjekkLengde(String passord)
    {
        return (passord.length() >= 10);

    }

    public static boolean sjekkForBareSifferOgTall(String passord)
    {
        boolean valid = true;
        for (int i = 0; i < passord.length() - 1; i++)
        {
            if (!Character.isLetterOrDigit(passord.charAt(i)))
            {
                valid = false;
                return valid;
            }
        }
        return valid;
    }

    public static boolean sjekkAtPassordHarMinstTreTall(String passord)
    {
        int antallSiffer = 0;
        for (int i = 0; i < passord.length() - 1; i++)
        {
            if (Character.isDigit(passord.charAt(i)))
            {
               antallSiffer++;
            }

        }
        return (antallSiffer>=3);
    }
    public static boolean sjekkPassord(String passord)
    {
      return (sjekkLengde(passord) && sjekkForBareSifferOgTall(passord) && sjekkAtPassordHarMinstTreTall(passord));
    }

}




package Oblig2019.EkteEllerUekteBrok;

import java.util.Scanner;

public class EkteEllerUekteBrok
{
    public static void main(String[] args)
    {
        Scanner input = new Scanner(System.in);

        System.out.println("Dette programmet verifiserer om brøken din er uekte eller ekte brøk");
        System.out.println("Skriv først inn tallet over brøkstreken, og så tallet under brøkstreken");
        int teller = input.nextInt();
        int nevner = input.nextInt();

        if (teller < nevner)
        {
            System.out.print("Dette er en ekte brøk! ");
            System.out.print(teller);
            System.out.print(" / ");
            System.out.println(nevner);
        }
        else
        {
            int heltall = teller / nevner;
            int nyTeller = teller % nevner;

            System.out.print("Dette er en uekte brøk! ");
            System.out.print(heltall);
            System.out.print(" og ");
            System.out.print(nyTeller);
            System.out.print(" / ");
            System.out.println(nevner);
        }
    }
}
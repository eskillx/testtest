package Oblig2019.TriangleExecptionOppgave;

public class Triangle extends SimpleGeometricObject
{
    private double side1;
    private double side2;
    private double side3;

    public Triangle()
    {
        super("Yellow", false);
        this.side1 = 1.0;
        this.side2 = 1.0;
        this.side3 = 1.0;


    }

    public Triangle(double sidelength)
    {
        super("Blue", true);
        this.side1 = sidelength;
        this.side2 = sidelength;
        this.side3 = sidelength;
    }

    public Triangle(double sidelength1, double sidelength2, double sidelength3)
    {
        super("Red", false);

        this.side1 = sidelength1;
        this.side2 = sidelength2;
        this.side3 = sidelength3;

    }


    public boolean checkLegality(double side1, double side2, double side3) throws IllegalTriangleExeption

    {
        if (side1 > 0 && side2 > 0 && side3 > 0 && side1 + side2 > side3
                && side1 + side3 > side2 && side3 + side2 > side1)
        {
            return true;
        }
        else
        {
            throw new IllegalTriangleExeption();
        }

        //Et triangel er et lovlig triangel dersom alle sider er større enn null,
        // og summen av hvilke som helst to sider er  lengre enn den tredje siden.
    }

    public double getSide1()
    {
        return side1;
    }

    public void setSide1(double side1)
    {
        this.side1 = side1;
    }

    public double getSide2()
    {
        return side2;
    }

    public void setSide2(double side2)
    {
        this.side2 = side2;
    }

    public double getSide3()
    {
        return side3;
    }

    public void setSide3(double side3)
    {
        this.side3 = side3;
    }

    public double getArea()
    {
        double s = (side1 + side2 + side3) / 2;
        return Math.sqrt(s * (s - side1) * (s - side2) * (s - side3));


    }

    public double getPerimiter()
    {
        return side1 + side2 + side3;
    }

    public boolean equals(Triangle other)
    {
        return this.side1 == other.getSide1() && this.side2 == other.getSide2() && this.side3 == other.getSide3();
    }


    @Override
    public String toString()
    {
        return "Sides: " + String.format("%.2f, %.2f, and %.2f\n", side1, side2, side3) + super.toString();

    }
}



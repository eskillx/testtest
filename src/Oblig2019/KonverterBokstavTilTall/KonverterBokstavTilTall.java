package Oblig2019.KonverterBokstavTilTall;

import java.util.Scanner;

public class KonverterBokstavTilTall
{
    public static void main(String[] args)
    {
        Scanner input = new Scanner(System.in);
        System.out.println("Dette programmet gjør om en bokstavkarakter til en tallkarakter");
        System.out.println("Venligst skriv inn bokstavkarakteren din: ");
        String grade = input.next().toLowerCase();

        switch (grade)
        {
            case ("a"):
                System.out.println("Karakter A tilsvarer 6");
                break;
            case ("b"):
                System.out.println("Karakter B tilsvarer 5");
                break;
            case ("c"):
                System.out.println("Karakter C tilsvarer 4");
                break;
            case ("d"):
                System.out.println("karakter D tilsvarer 3");
                break;
            case ("e"):
                System.out.println("Karakter E tilsvarer 2");
                break;
            case ("f"):
                System.out.println("Karakter F tilsvarer 1 eller stryk");
                break;
            default:
                System.out.println("Du har ikke oppgitt en gyldig karakter...");

        }
    }
}

package Oblig2019.KonverterBokstavTilTall;

import java.util.Scanner;

public class KonverterBokstavTilTallV2
{
    public static void main(String[] args)
    {
        Scanner input = new Scanner(System.in);
        System.out.println("Dette programmet gjør om en bokstav til en tall på telefon");
        System.out.println("Venligst skriv inn bokstaven din: ");
        String character = input.next().toLowerCase();
        int number = 123; // satt til tall som ikke er på telefontastatur

        if (character.matches("[abc]"))
        {
            number = 2;
        }
        else if (character.matches("[def]"))
        {
            number = 3;
        }
        else if (character.matches("[ghi]"))
        {
            number = 4;
        }
        else if (character.matches("[jkl]"))
        {
            number = 5;
        }
        else if (character.matches("[mno]"))
        {
            number = 6;
        }
        else if (character.matches("[pqrs]"))
        {
            number = 7;
        }
        else if (character.matches("[tuv]"))
        {
            number = 8;
        }
        else if (character.matches("[wxyz]"))
        {
            number = 9;
        }
        else if (character.matches(" "))
        {
            number = 0;
        }
        else
        {
            System.out.println("Dette er ikke en bokstav");
        }
        if (number >= 0 && number <= 9)
        {
            System.out.println("Bokstav " + character + " tilsvarer tall " + number + " på mobil tastatur");
        }
        else
        {
            System.out.println("Prøv igjen.");
        }
    }
}

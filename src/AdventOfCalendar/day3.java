package AdventOfCalendar;

import java.util.ArrayList;

public class day3
{
    public static void main(String[] args)
    {
        String input1 = "R75,D30,R83,U83,L12,D49,R71,U7,L72";
        String input2 = "U62,R66,U55,R34,D71,R55,D58,R83";
        String[] wirepath1 = input1.split(",");
        String[] wirepath2 = input2.split(",");
        ArrayList<String> intersections = new ArrayList<>();
        ArrayList<String> paths1 = new ArrayList<>();
        ArrayList<String> paths2 = new ArrayList<>();
        int x1 = 0;
        int x2 = 0;
        int y1 = 0;
        int y2 = 0;
        String shortIsc = "";
        int distToShortestIsc;

        for (int i = 0; i < wirepath1.length; i++)
        {
            switch (direction(wirepath1[i]))
            {
                case 'U':
                    for (int j = 0; j < distance(wirepath1[i]); j++)
                    {
                        y1++;
                        paths1.add(String.format("%d,%d", x1, y1));
                    }

                    break;
                case 'D':
                    for (int j = 0; j < distance(wirepath1[i]); j++)
                    {
                        y1--;
                        paths1.add(String.format("%d,%d", x1, y1));

                    }

                    break;
                case 'R':
                    for (int j = 0; j < distance(wirepath1[i]); j++)
                    {
                        x1++;
                        paths1.add(String.format("%d,%d", x1, y1));

                    }

                    break;
                case 'L':
                    for (int j = 0; j < distance(wirepath1[i]); j++)
                    {
                        x1--;
                        paths1.add(String.format("%d,%d", x1, y1));

                    }

                    break;
            }
        }
        for (int i = 0; i < wirepath2.length; i++)
        {
            switch (direction(wirepath2[i]))
            {
                case 'U':
                    for (int j = 0; j < distance(wirepath2[i]); j++)
                    {
                        y2++;
                        paths2.add(String.format("%d,%d", x2, y2));

                    }

                    break;
                case 'D':
                    for (int j = 0; j < distance(wirepath2[i]); j++)
                    {
                        y2--;
                        paths2.add(String.format("%d,%d", x2, y2));
                    }

                    break;
                case 'R':
                    for (int j = 0; j < distance(wirepath2[i]); j++)
                    {
                        x2++;
                        paths2.add(String.format("%d,%d", x2, y2));
                    }

                    break;
                case 'L':
                    for (int j = 0; j < distance(wirepath2[i]); j++)
                    {
                        x2--;
                        paths2.add(String.format("%d,%d", x2, y2));
                    }

                    break;
            }
        }


        for (int i = 0; i < paths1.size(); i++)
        {
            for (int j = 0; j < paths2.size(); j++)
            {
                if (paths1.get(i).equals(paths2.get(j)))
                {
                    intersections.add(paths1.get(i));
                }
            }
        }

        for (int i = 0; i < intersections.size(); i++)
        {
            System.out.println(intersections.get(i));
        }

        distToShortestIsc=distanceManhattan(intersections.get(0));
        shortIsc=intersections.get(0);
        for (int i = 0; i < intersections.size(); i++)
        {

            if (distToShortestIsc>distanceManhattan(intersections.get(i)))
            {
                shortIsc = intersections.get(i);
                distToShortestIsc= distanceManhattan(intersections.get(i));
            }
        }
        System.out.printf("The closest intersection point is %s, and the distance is %d",shortIsc,distToShortestIsc);
    }


    public static char direction(String wirepath)
    {
        return wirepath.charAt(0);


    }

    public static int distance(String wirepath)
    {
        String extractedDistance = wirepath.replaceAll("[^0-9]", "");
        return Integer.parseInt(extractedDistance);
    }
    public static int distanceManhattan (String xyco)
    {
        String[] temp = xyco.split(",");
        return Math.abs(Integer.parseInt(temp[0])) + Math.abs(Integer.parseInt(temp[1]));

    }

}


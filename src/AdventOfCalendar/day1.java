package AdventOfCalendar;

import java.util.ArrayList;

public class day1
{
    public static void main(String[] args)
    {
        ArrayList<Integer> spacecraft = new ArrayList<>();
        int totalFuel =0;
        spacecraft.add(fuelNeeded(12));
        spacecraft.add(fuelNeeded(14));
        spacecraft.add(fuelNeeded(1969));
        spacecraft.add(fuelNeeded(100756));

        for (int i = 0; i < spacecraft.size();i++)
        {
            totalFuel += spacecraft.get(i);
        }
        System.out.println(totalFuel);
    }

    public static int fuelNeeded(int moduleMass)
    {
        return (moduleMass / 3) - 2;

    }
}


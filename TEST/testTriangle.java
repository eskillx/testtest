import org.junit.jupiter.api.Test;
import Oblig2019.Triangleklassen.Triangle;

import static org.junit.jupiter.api.Assertions.*;

public class testTriangle
{


    @Test
    void testEquals()
    {
        Triangle triangle1 = new Triangle(1, 2, 3);
        Triangle triangle2 = new Triangle(1, 2, 3);
        Triangle triangle3 = new Triangle(4, 3, 5);

        assertEquals(true, triangle1.equals(triangle2));
        assertEquals(false, triangle1.equals(triangle3));
        assertEquals(false, triangle2.equals(triangle3));

    }

    @Test
    void testCheckLegality()
    {
        Triangle triangle1 = new Triangle(1, 2, 3);
        assertEquals(true, triangle1.checkLegality(1, 1, 1));
        assertEquals(false, triangle1.checkLegality(1, 2, 3));
        assertEquals(false, triangle1.checkLegality(3, 4, 12));

    } @Test
    void testgetArea()
    {
        Triangle triangle1 = new Triangle(3, 4, 5);
        Triangle triangle2 = new Triangle(9, 12, 15);

        assertEquals(6, triangle1.getArea());
        assertEquals(54, triangle2.getArea());


    } @Test
    void testGetPerimiter()
    {
        Triangle triangle1 = new Triangle(3, 4, 5);
        Triangle triangle2 = new Triangle(9, 12, 15);

        assertEquals(12, triangle1.getPerimiter());
        assertEquals(36, triangle2.getPerimiter());

    }

}

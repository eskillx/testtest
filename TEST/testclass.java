import Oblig2019.Accountklassen.Account;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
public class testclass

{


    @Test
    void testWithdraw()
    {
        Account account1 = new Account(1121, 2500);

        assertEquals(false, account1.deposit(-2000));
        assertEquals(true, account1.deposit(2000));
        assertEquals(true, account1.withdraw(-2000));
        assertEquals(true, account1.withdraw(2000));
        assertEquals(false, account1.withdraw(5000));

    }

}

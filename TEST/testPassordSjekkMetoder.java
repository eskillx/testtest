import org.junit.jupiter.api.Test;
import Oblig2019.EnhetstestForMetodePassord.PassordSjekkMetoder;

import static org.junit.jupiter.api.Assertions.*;

public class testPassordSjekkMetoder

{


    @Test
    void testPassordUtentall()
    {
        assertFalse(PassordSjekkMetoder.sjekkPassord("ABCDEFGHIJKLMOPQ"));
        ;
    }

    @Test
    void testPassordMedSpesialtegn()
    {
        assertFalse(PassordSjekkMetoder.sjekkPassord(" ASDJI_wroi23rj23afas"));
    }

    @Test
    void testForKortPassord()
    {
        assertFalse(PassordSjekkMetoder.sjekkPassord("abc123AVC"));
    }

    @Test
    void testGodkjentepassord()
    {
        assertTrue(PassordSjekkMetoder.sjekkPassord("12355679010"));
        assertTrue(PassordSjekkMetoder.sjekkPassord("ABC123DEF987"));
        assertTrue(PassordSjekkMetoder.sjekkPassord("abcqoweiru1234lksajd"));
        assertTrue(PassordSjekkMetoder.sjekkPassord("ajwefoeiwsadfasdfasdf1234"));
    }


}

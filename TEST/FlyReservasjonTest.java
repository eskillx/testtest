import Oblig2019.Flyreservasjon.FlyReservasjon;
import Oblig2019.Flyreservasjon.Rad;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class FlyReservasjonTest
{
    @Test
    void testRaderOpprettesRiktig()
    {
        Rad Rad1 = new Rad(12);
        assertEquals(10, Rad1.getNumberOfSeats());
        Rad Rad2 = new Rad(10);
        assertTrue((Rad2.getSeatAtIndex(0) == 'A'));
        assertTrue((Rad2.getSeatAtIndex(1) == 'B'));
        assertTrue((Rad2.getSeatAtIndex(2) == 'C'));
        assertTrue((Rad2.getSeatAtIndex(3) == 'D'));
        assertTrue((Rad2.getSeatAtIndex(4) == 'E'));
        assertTrue((Rad2.getSeatAtIndex(5) == 'F'));
        assertTrue((Rad2.getSeatAtIndex(6) == 'G'));
        assertTrue((Rad2.getSeatAtIndex(7) == 'H'));
        assertTrue((Rad2.getSeatAtIndex(8) == 'I'));
        assertTrue((Rad2.getSeatAtIndex(9) == 'J'));
    }

    @Test
    void testErGyldigSete()
    {
        Rad Rad1 = new Rad(4);
        assertTrue(Rad1.isLegalSeat('A'));
        assertTrue(Rad1.isLegalSeat('D'));
        assertFalse(Rad1.isLegalSeat('F'));
    }

    @Test
    void testTilordneSete()
    {
        Rad Rad1 = new Rad(4);
        assertTrue(Rad1.assignSeat('A'));
        assertEquals('X', Rad1.getSeatAtIndex(0));
        assertTrue(Rad1.assignSeat('D'));
        assertFalse(Rad1.assignSeat('F'));
        assertFalse(Rad1.assignSeat('A'));
        assertFalse(Rad1.assignSeat('D'));
    }

    @Test
    void testRadPrint()
    {
        Rad Rad1 = new Rad(4);
        Rad1.printSeats();

    }
    @Test
    void testRadOk()
    {
        FlyReservasjon Fly1 = new FlyReservasjon(10);
        assertFalse(Fly1.radOk(10));
        assertTrue(Fly1.radOk(9));
        assertTrue(Fly1.radOk(0));
    }

@Test
    void    testFlyReservasjonShow()
{
    FlyReservasjon Fly1 = new FlyReservasjon(10);
    Fly1.show();

}
    @Test
    void    testFlyReservasjonReserverSete()
    {
        FlyReservasjon Fly1 = new FlyReservasjon(10);
        assertFalse(Fly1.reserveSeat(11,'B'));
        assertFalse(Fly1.reserveSeat(5,'N'));
        assertTrue(Fly1.reserveSeat(5,'B'));


    }



}


